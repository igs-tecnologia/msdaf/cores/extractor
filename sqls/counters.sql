-- Count amount of "Dispensacao" between '2019-05-01' AND "DT_DISPENSACAO" < DATE '2019-07-31' = 58.296.624 records
SELECT COUNT(DT_DISPENSACAO) as amount
FROM DBDMBNAFAR.VW_DISPENSACAO
WHERE "DT_DISPENSACAO" >= DATE '2018-01-01' AND "DT_DISPENSACAO" < DATE '2018-02-01' AND CO_ORIGEM_REGISTRO LIKE 'F';
