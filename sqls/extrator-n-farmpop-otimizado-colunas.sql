-- Agrupar por: Municipio, UF, QtdDipensada mes, mes competencia, 
SELECT
-- mes-ano
NU_COMPETENCIA_DISPENSACAO,
-- Dados do Produto
NU_CATMAT, CO_PRINCIPIO_ATIVO_MEDICAMENTO,
-- Dispensacao
MAX(VL_UNITARIO) AS VL_UNITARIO, SUM(QT_DISPENSACAO) AS MONTANTE_DISPENSADA,
-- GeoPaciente
CO_MUNICIPIO_IBGE_PAC, CO_MICRORREGIONAL_SAUDE_PAC
-- Fonte Produto
NU_REGISTRO_ANVISA,
-- Financeiro do Produto
CO_GRUPO_FINANCIAMENTO,
-- Programa do Produto
SG_PROGRAMA_SAUDE,
-- Dados adicionais de GeoLoc do Estabelecimento (Farmacia)
CO_MUNICIPIO_IBGE_EST, CO_MICRORREGIONAL_SAUDE_EST

FROM DBDMBNAFAR.VW_DISPENSACAO
WHERE "NU_COMPETENCIA_DISPENSACAO" IS NOT NULL AND "NU_COMPETENCIA_DISPENSACAO" NOT LIKE -1 --AND ROWNUM <= 10
GROUP BY 
-- Localização
NU_COMPETENCIA_DISPENSACAO, CO_MUNICIPIO_IBGE_PAC, CO_MICRORREGIONAL_SAUDE_PAC,
-- Dados do Produto
NU_CATMAT, CO_PRINCIPIO_ATIVO_MEDICAMENTO, NU_REGISTRO_ANVISA,
-- Financeiro/Programa do Produto
CO_GRUPO_FINANCIAMENTO, SG_PROGRAMA_SAUDE,
-- Dados adicionais de GeoLoc do Estabelecimento (Farmacia)
CO_MUNICIPIO_IBGE_EST, CO_MICRORREGIONAL_SAUDE_EST;